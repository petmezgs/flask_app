To run the Flask app please follow the steps below. It is recommended to use cmd.

1) Create and activate a virtual environment using either the YAML file located in the main project directory (i.e., "conda env create -f flask_env.yml") or the requirements.txt file (i.e., "conda create --name flask_env --file requirements.txt
").

2) Activate the environment (i.e., "conda activate flask_env").

3) The app uses a certain Docker image to analyze the input video files. Pull the Docker image either from Docker Hub using the following command: "docker pull petmezgs/video_df:v1.2" or the GitLab repository (the image is called "video-deepfake-detection").

4) Navigate to the local Flask project directory using cd, i.e., "cd /path/to/flask/project".

5) Run the Flask app through the bash script called "run_app.sh". To execute it run "./run_app.sh". It will run the Flask app with sudo privileges. The app requires sudo permissions to perform file creation and deletion processes inside the project subdirectories.

6) Please remember: the video files must be at least 4 seconds long!

7) To terminate the app type "Ctrl + C".

8) Enjoy!