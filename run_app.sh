#!/bin/bash

# Change permissions
sudo chmod -R 777 ./static/

# Run your Flask app
python3 app.py
