from flask import Flask, render_template, request, redirect, url_for
import os
import subprocess
import shutil
import json

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = './static/uploads'
app.config['OUTPUT_FOLDER'] = './static/output'

if not os.path.exists(app.config['UPLOAD_FOLDER']):
    os.makedirs(app.config['UPLOAD_FOLDER'])
if not os.path.exists(app.config['OUTPUT_FOLDER']):
    os.makedirs(app.config['OUTPUT_FOLDER'])

def delete_and_create_folder(folder_path):
    # Remove the folder using 'sudo rm -rf'
    subprocess.run(['sudo', 'rm', '-rf', folder_path], check=True)
    # Create the folder
    os.makedirs(folder_path)

@app.route('/')
def upload_file():
    return render_template('upload.html')

@app.route('/upload', methods=['POST'])
def upload():
    selected_files = request.files.getlist("file[]")
    
    # Check if at least two files are uploaded
    if len(selected_files) < 2:
        return render_template('upload.html', error="Please upload at least two video files.")

    for file in selected_files:
        if file.filename != '':
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], file.filename))

    return redirect(url_for('select_reference'))

@app.route('/select_reference')
def select_reference():
    uploaded_files = os.listdir(app.config['UPLOAD_FOLDER'])
    return render_template('select_reference.html', files=uploaded_files)

@app.route('/rename', methods=['GET', 'POST'])
def rename():
    reference_files = request.form.getlist("reference_file")

    reference_count = 1
    test_count = 1

    # Check if at least one file is selected as reference
    if not reference_files:
        return render_template('select_reference.html', files=os.listdir(app.config['UPLOAD_FOLDER']), error="Please select at least one video file as Reference.")

    renaming_dict = {}  # Initialize the dictionary

    for filename in sorted(os.listdir(app.config['UPLOAD_FOLDER'])):
        if filename in reference_files:
            new_filename = f"Reference{reference_count}.mp4"
            reference_count += 1
        else:
            new_filename = f"Test{test_count}.mp4"
            test_count += 1

        renaming_dict[filename] = new_filename

        os.rename(os.path.join(app.config['UPLOAD_FOLDER'], filename), os.path.join(app.config['UPLOAD_FOLDER'], new_filename))

    # Save the dictionary as a text file
    dict_path = os.path.join(app.config['OUTPUT_FOLDER'], 'renaming_dict.txt')
    with open(dict_path, 'w') as dict_file:
        json.dump(renaming_dict, dict_file)

    # Pass the renaming_dict as a parameter to make_predictions
    return redirect(url_for('make_predictions'))

@app.route('/make_predictions')
def make_predictions():
    # Read renaming_dict from the locally generated text file
    dict_path = os.path.join(app.config['OUTPUT_FOLDER'], 'renaming_dict.txt')
    with open(dict_path, 'r') as dict_file:
        renaming_dict = json.load(dict_file)

    docker_command = [
        'docker', 'run', '--gpus', 'all', '--rm', '-it',
        '-v', f'{os.getcwd()}/static/uploads/:/app/data/input/',
        '-v', f'{os.getcwd()}/static/output/:/app/data/output/',
        'petmezgs/video_df:v1.2'
    ]
    try:
        subprocess.run(docker_command, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        # Read the contents of results.txt after Docker run
        results_path = os.path.join(app.config['OUTPUT_FOLDER'], 'results.txt')
        with open(results_path, 'r') as results_file:
            #results_content = results_file.read()
            # Extract only the relevant information from each line
            results_content = [line.split(" for ")[1].split(":")[0].strip() + ": " + line.split(":")[-1].strip() for line in results_file if "Test" in line]

        return render_template('make_predictions.html', results=results_content, renaming_dict=renaming_dict)
    except subprocess.CalledProcessError as e:
        # Print Docker command output for debugging
        print(f"Docker output:\n{e.stdout.decode()}\n{e.stderr.decode()}")
        return f"Error: {e}"

if __name__ == '__main__':
    delete_and_create_folder(app.config['UPLOAD_FOLDER'])
    delete_and_create_folder(app.config['OUTPUT_FOLDER'])
    app.run(debug=True)